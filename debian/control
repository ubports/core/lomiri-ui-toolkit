Source: lomiri-ui-toolkit
Priority: optional
Section: libs
Maintainer: UBports Developers <developers@ubports.com>
Build-Depends: accountsservice <!nocheck>,
               lomiri-schemas <!nocheck>,
               dbus <!nocheck>,
               dbus-test-runner <!nocheck>,
               debhelper-compat (= 13),
               devscripts,
               dh-migrations | hello,
               dh-sequence-python3,
               gdb,
               language-pack-en-base <!cross> | hello,
               libdbus-1-dev,
               libevdev-dev,
               libfontconfig1-dev,
               libfreetype6-dev,
               libgl1-mesa-dri,
               libgles2-mesa-dev,
               libglib2.0-dev,
               libinput-dev,
               liblttng-ust-dev,
               libmtdev-dev,
               libqt5sql5-sqlite,
               libqt5svg5-dev,
               libudev-dev,
               libx11-dev,
               libxcb1-dev,
               libxi-dev,
               libxkbcommon-dev,
               libxrender-dev,
               locales,
               lsb-release,
               pycodestyle <!cross> | pep8 <!cross>,
               python3-all:any,
               python3-debian <!cross>,
               python3-sphinx <!cross>,
               qml-module-qt-labs-settings,
               qml-module-qtfeedback,
               qml-module-qtgraphicaleffects,
               qml-module-qtqml-models2,
               qml-module-qtquick-layouts,
               qml-module-qtquick-localstorage,
               qml-module-qtquick-window2,
               qml-module-qtquick2,
               qml-module-qttest,
               qtbase5-dev,
               qtbase5-private-dev,
               qtdeclarative5-dev (>= 5.12.8~),
               qtdeclarative5-dev-tools,
               qtdeclarative5-doc-html <!cross>,
               qtdeclarative5-private-dev,
               qtmultimedia5-doc-html <!cross>,
               qtpim5-dev (>= 5.0~git20171109~0bd985b),
               qtscript5-doc-html <!cross>,
               qtsvg5-doc-html <!cross>,
               qtsystems5-dev,
               qtsystems5-private-dev,
               qttools5-dev-tools,
               suru-icon-theme (>= 14.04+16.10.20160720),
               uuid-runtime,
               xauth <!nocheck>,
               xvfb <!nocheck>,
Standards-Version: 3.9.7
Homepage: https://launchpad.net/lomiri-ui-toolkit
Vcs-Bzr: https://code.launchpad.net/~lomiri-sdk-team/lomiri-ui-toolkit/trunk

Package: qml-module-lomiri-components
Architecture: any
Multi-Arch: same
Depends: ${fonts-ubuntu},
         liblomirigestures5t64 (= ${binary:Version}),
         liblomirimetrics5t64 (= ${binary:Version}),
         liblomiritoolkit5t64 (= ${binary:Version}),
         libqt5svg5,
         lomiri-ui-toolkit-common (>= ${source:Version}),
            lomiri-ui-toolkit-common (<< ${source:Version}.1~),
         lomiri-ui-toolkit-theme (= ${binary:Version}),
         qml-module-lomiri-components-labs,
         qml-module-lomiri-performancemetrics,
         qml-module-qtfeedback,
         qml-module-qtgraphicaleffects,
         qml-module-qtquick-layouts,
         qml-module-qtquick-window2,
         qml-module-qtquick2,
         suru-icon-theme (>= 14.04+16.10.20160720),
         ${misc:Depends},
         ${shlibs:Depends},
Conflicts: qt-components-lomiri,
Breaks: qtdeclarative5-lomiri-ui-toolkit-plugin (<< ${source:Version}),
Replaces: qt-components-lomiri,
          qtdeclarative5-lomiri-ui-toolkit-plugin (<< ${source:Version}),
Provides: qt-components-lomiri,
Description: Qt Components for Lomiri - Components QML plugin
 Qt Components for Lomiri offers a set of reusable user interface
 components for Qt Quick 2 / QML.
 .
 This package contains the Lomiri Components QML plugin.

Package: qml-module-lomiri-components-labs
Architecture: any
Multi-Arch: same
Depends: ${fonts-ubuntu},
         liblomirigestures5t64 (= ${binary:Version}),
         liblomirimetrics5t64 (= ${binary:Version}),
         liblomiritoolkit5t64 (= ${binary:Version}),
         libqt5svg5,
         lomiri-ui-toolkit-theme (= ${binary:Version}),
         qml-module-lomiri-performancemetrics,
         qml-module-qtfeedback,
         qml-module-qtgraphicaleffects,
         qml-module-qtquick-layouts,
         qml-module-qtquick-window2,
         qml-module-qtquick2,
         suru-icon-theme,
         ${misc:Depends},
         ${shlibs:Depends},
Conflicts: qt-components-lomiri-labs,
Replaces: qt-components-lomiri-labs,
Provides: qt-components-lomiri-labs,
Description: Qt Components Labs for Lomiri - Components QML plugin
 Qt Components Labs for Lomiri offers a set of non-released and mostly
 unstable user interface components for Qt Quick 2 / QML.
 .
 This package contains the Lomiri Components Labs QML plugin.

Package: qml-module-lomiri-layouts
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
Breaks: qtdeclarative5-lomiri-ui-toolkit-plugin (<< ${source:Version}),
Replaces: qtdeclarative5-lomiri-ui-toolkit-plugin (<< ${source:Version}),
Description: Qt Components for Lomiri - Layouts QML plugin
 Qt Components for Lomiri offers a set of reusable user interface
 components for Qt Quick 2 / QML.
 .
 This package contains the Lomiri Layouts QML plugin.

Package: qml-module-lomiri-test
Architecture: any
Multi-Arch: same
Depends: qml-module-lomiri-components,
         qml-module-qttest | qtdeclarative5-test-plugin,
         ${misc:Depends},
         ${shlibs:Depends},
Breaks: qtdeclarative5-lomiri-ui-toolkit-plugin (<< ${source:Version}),
Replaces: qtdeclarative5-lomiri-ui-toolkit-plugin (<< ${source:Version}),
Description: Qt Components for Lomiri - Test QML plugin
 Qt Components for Lomiri offers a set of reusable user interface
 components for Qt Quick 2 / QML.
 .
 This package contains the Lomiri Test QML plugin.

Package: qml-module-lomiri-performancemetrics
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
Breaks: qtdeclarative5-lomiri-ui-toolkit-plugin (<< ${source:Version}),
Replaces: qtdeclarative5-lomiri-ui-toolkit-plugin (<< ${source:Version}),
Description: Qt Components for Lomiri - PerformanceMetrics QML plugin
 Qt Components for Lomiri offers a set of reusable user interface
 components for Qt Quick 2 / QML.
 .
 This package contains the Lomiri PerformanceMetrics QML plugin.

Package: qml-module-lomiri-metrics
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
Breaks: qtdeclarative5-lomiri-ui-toolkit-plugin (<< ${source:Version}),
Replaces: qtdeclarative5-lomiri-ui-toolkit-plugin (<< ${source:Version}),
Description: Qt Components for Lomiri - Metrics QML plugin
 Qt Components for Lomiri offers a set of reusable user interface
 components for Qt Quick 2 / QML.
 .
 This package contains the Lomiri Metrics QML plugin.

Package: qtdeclarative5-lomiri-ui-toolkit-plugin
Architecture: any
Section: oldlibs
Depends: qml-module-lomiri-components,
         qml-module-lomiri-layouts,
         qml-module-lomiri-performancemetrics,
         qml-module-lomiri-test,
         ${misc:Depends},
Description: Transitional dummy package for Lomiri UI Toolkit QML plugin
 This package contains the Lomiri Components QML plugin.

Package: liblomirigestures5t64
Provides: ${t64:Provides}
Replaces: liblomirigestures5
Breaks: liblomirigestures5 (<< ${source:Version})
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
Description: Lomiri gestures library for Lomiri UI Toolkit
 Library supporting Gesture recognition for Lomiri UI Toolkit
 Classes for SwipeArea, and for Lomiri Gestures in general.

Package: liblomirigestures5-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: liblomirigestures5t64 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Description: Lomiri gestures library development files
 This package contains the development files for
 Lomiri gestures library with SwipeArea

Package: liblomirigestures5-private-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: liblomirigestures5-dev (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Description: Lomiri gestures library private development files
 This package contains the private development files for
 Lomiri gestures library with SwipeArea

Package: liblomirimetrics5t64
Provides: ${t64:Provides}
Replaces: liblomirimetrics5
Breaks: liblomirimetrics5 (<< ${source:Version})
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
Description: Lomiri metrics library for Lomiri UI Toolkit
 Library for monitoring QtQuick 2 applications

Package: liblomirimetrics5-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: liblomirimetrics5t64 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Description: Lomiri metrics library development files
 This package contains the development files for the
 Lomiri metrics library

Package: liblomirimetrics5-private-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: liblomirimetrics5-dev (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Description: Lomiri metrics library private development files
 This package contains the private development files for
 the Lomiri metrics library

Package: liblomiritoolkit5t64
Provides: ${t64:Provides}
Replaces: liblomiritoolkit5
Breaks: liblomiritoolkit5 (<< ${source:Version})
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
Description: Lomiri toolkit common library for Lomiri UI Toolkit
 Contains components and helper classes that are shared between the
 Lomiri QML plugins

Package: liblomiritoolkit5-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: liblomiritoolkit5t64 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Description: Lomiri toolkit common library development files
 This package contains the development files for
 Lomiri toolkit common library

Package: liblomiritoolkit5-private-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: liblomiritoolkit5-dev (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Description: Lomiri toolkit common library private development files
 This package contains the private development files for
 Lomiri toolkit common library

Package: lomiri-ui-toolkit-common
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
         ${shlibs:Depends},
Breaks: qml-module-lomiri-components (<< 1.3.5011~),
Replaces: qml-module-lomiri-components (<< 1.3.5011~),
Description: Qt Components for Lomiri - common files
 Qt Components for Lomiri offers a set of reusable user interface
 components for Qt Quick 2 / QML.
 .
 This package contains the common file across multiple architectures. At the
 moment, it contains only session migration script.

Package: lomiri-ui-toolkit-theme
Architecture: any
Multi-Arch: foreign
Depends: ${misc:Depends},
         ${shlibs:Depends},
Replaces: qt-components-lomiri,
Description: Qt Components for Lomiri - Lomiri Theme
 Qt Components for Lomiri offers a set of reusable user interface
 components for Qt Quick 2 / QML.
 .
 This package contains the Ambiance theme for Qt Lomiri Components.

Package: lomiri-ui-toolkit-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Build-Profiles: <!cross>
Depends: ${misc:Depends},
         ${shlibs:Depends},
Suggests: qtdeclarative5-doc-html,
          qtmultimedia5-doc-html,
          qtscript5-doc-html,
          qtsvg5-doc-html,
Conflicts: qt-components-lomiri-doc,
Replaces: qt-components-lomiri-doc,
Provides: qt-components-lomiri-doc,
Description: Qt Components for Lomiri - documentation
 Qt Components for Lomiri offers a set of reusable user interface
 components for Qt Quick 2 / QML.
 .
 This package contains developer documentation.

Package: lomiri-ui-toolkit-examples
Section: devel
Architecture: any
Depends: lomiri-ui-toolkit-theme (= ${binary:Version}),
         lomiri-ui-toolkit-tools (>= ${source:Version}),
         qml-module-lomiri-components (= ${binary:Version}),
         qml-module-qt-labs-folderlistmodel,
         qml-module-qtqml-models2,
         qml-module-qtquick-xmllistmodel,
         ${misc:Depends},
         ${shlibs:Depends},
Recommends: libqt5multimedia5-plugins,
            lomiri-ui-toolkit-doc,
Conflicts: qt-components-lomiri-demos,
           qt-components-lomiri-examples,
Replaces: qt-components-lomiri-demos,
          qt-components-lomiri-examples,
Provides: qt-components-lomiri-demos,
          qt-components-lomiri-examples,
Description: Qt Components for Lomiri - demos and examples
 Qt Components for Lomiri offers a set of reusable user interface
 components for Qt Quick 2 / QML.
 .
 This package contains the component demos and example applications.

Package: lomiri-ui-toolkit-autopilot
Architecture: any
Depends: autopilot-qt5,
         dpkg-dev,
         libqt5test5,
         libqt5widgets5,
         lomiri-app-launch-profiler (>= ${source:Version}),
         lomiri-keyboard-autopilot [amd64 armhf i386],
         lomiri-ui-toolkit-examples (>= ${source:Version}),
         python3-autopilot (>= 1.4) [amd64 armhf arm64 i386 ppc64 ppc64el],
         python3-autopilot-trace [amd64 armhf arm64 i386 ppc64 ppc64el],
         python3-fixtures,
         python3-gi,
         python3-testscenarios,
         python3-testtools,
         qml-module-lomiri-components (>= ${source:Version}),
         qml-module-qttest,
         qttestability-autopilot [amd64 armhf arm64 i386 ppc64 ppc64el],
         upstart [amd64 armhf arm64 i386 ppc64 ppc64el],
         url-dispatcher-tools [amd64 armhf arm64 i386 ppc64 ppc64el],
         ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends},
Description: Test package for Lomiri UI Toolkit
 Autopilot tests for the lomiri-ui-toolkit package

Package: lomiri-app-launch-profiler
Architecture: any
Depends: lttng-tools,
         python3-babeltrace,
         ${misc:Depends},
         ${shlibs:Depends},
Description: Qt Components for Lomiri - startup time profiling tool
 Qt Components for Lomiri offers a set of reusable user interface
 components for Qt Quick 2 / QML.
 .
 This package contains the application startup time profiling tools.

Package: lomiri-ui-toolkit-tools
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
Description: Qt Components for Lomiri - productive tools
 Qt Components for Lomiri offers a set of reusable user interface
 components for Qt Quick 2 / QML.
 .
 This package contains the application launcher, which is a drop-in
 replacement for qmlscene, oxideqmlscene, qml and derivatives.

Package: qml-module-ubuntu-components
Architecture: any
Multi-Arch: same
Depends: liblomirigestures5t64 (= ${binary:Version}),
         liblomirimetrics5t64 (= ${binary:Version}),
         liblomiritoolkit5t64 (= ${binary:Version}),
         ubuntu-ui-toolkit-theme (= ${binary:Version}),
         qml-module-lomiri-components (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Breaks: qml-module-ubuntu-components (<< 1.3.3000),
Replaces: qml-module-ubuntu-components (<< 1.3.3000),
Description: Qt Components for Lomiri - Ubuntu compatiblity layer
 Qt Components for Lomiri offers a set of reusable user interface
 components for Qt Quick 2 / QML.
 .
 This package contains the Ubuntu Components QML plugin, implemented as
 compatibility layers over Lomiri Components.

Package: qml-module-ubuntu-components-labs
Architecture: any   
Multi-Arch: same
Depends: qml-module-lomiri-components-labs (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Breaks: qml-module-ubuntu-components-labs (<< 1.3.3000)
Replaces: qml-module-ubuntu-components-labs (<< 1.3.3000)
Description: Qt Components Labs for Lomiri - Ubuntu compatibility layer
 Qt Components Labs for Lomiri offers a set of non-released and mostly
 unstable user interface components for Qt Quick 2 / QML.
 .
 This package contains the Ubuntu Components Labs QML plugin, implemented
 as compatibility layers over Lomiri Components.

Package: qml-module-ubuntu-layouts
Architecture: any
Multi-Arch: same
Depends: qml-module-lomiri-layouts (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Breaks: qml-module-ubuntu-layouts (<< 1.3.3000),
Replaces: qml-module-ubuntu-layouts (<< 1.3.3000),
Description: Qt Components for Lomiri - Ubuntu.Layouts compat layer
 Qt Components for Lomiri offers a set of reusable user interface
 components for Qt Quick 2 / QML.
 .
 This package contains the Ubuntu Layouts QML plugin, implemented as
 compatibility layers over Lomiri Components.

Package: qml-module-ubuntu-performancemetrics
Architecture: any
Multi-Arch: same
Depends: qml-module-lomiri-performancemetrics (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Breaks: qml-module-ubuntu-performancemetrics (<< 1.3.3000),
Replaces: qml-module-ubuntu-performancemetrics (<< 1.3.3000),
Description: Qt Components for Lomiri - Ubuntu.PerformanceMetrics
 Qt Components for Lomiri offers a set of reusable user interface
 components for Qt Quick 2 / QML.
 .
 This package contains the Ubuntu PerformanceMetrics QML plugin,
 implemented as compatibility layers over Lomiri Components.

Package: qml-module-ubuntu-metrics
Architecture: any
Multi-Arch: same
Depends: qml-module-lomiri-metrics (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Breaks: qml-module-ubuntu-metrics (<< 1.3.3000),
Replaces: qml-module-ubuntu-metrics (<< 1.3.3000),
Description: Qt Components for Lomiri - Ubuntu.Metrics compat layer
 Qt Components for Lomiri offers a set of reusable user interface
 components for Qt Quick 2 / QML.
 .
 This package contains the Ubuntu Metrics QML plugin, implemented
 as compatibility layers over Lomiri Components.

Package: qml-module-ubuntu-test
Architecture: any
Multi-Arch: same
Depends: qml-module-lomiri-test,
         ${misc:Depends},
         ${shlibs:Depends},
Description: Qt Components for Lomiri - Test QML plugin
 Qt Components for Lomiri offers a set of reusable user interface
 components for Qt Quick 2 / QML.
 .
 This package contains the Ubuntu Test QML plugin, implemented
 as compatibility layers over Lomiri Components.

Package: ubuntu-ui-toolkit-theme
Architecture: any
Multi-Arch: foreign
Depends: lomiri-ui-toolkit-theme (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Breaks: ubuntu-ui-toolkit-theme (<< 1.3.3000),
Replaces: ubuntu-ui-toolkit-theme (<< 1.3.3000),
Description: Qt Components for Lomiri - Lomiri Theme
 Qt Components for Lomiri offers a set of reusable user interface
 components for Qt Quick 2 / QML.
 .
 This package contains the Ubuntu compatibility themes for Qt Lomiri
 Components.
